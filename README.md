# playbook-for-pipenv
Ansible playbook to install pyenv and pipenv.
It expects to run on CentOS 8 or AlmaLinux 8  
because commands include dnf and systemctl.

## Usage
```
ansible-playbook -i inventory/hosts docker.yml
```
